#ifndef _BELLARDCOMPLETIONAPI_HPP
#define _BELLARDCOMPLETIONAPI_HPP
#define BOOST_ASIO_HAS_CO_AWAIT
#include <nlohmann/json.hpp>
#include <boost/asio.hpp>
#include <boost/beast.hpp>
#include <boost/beast/ssl.hpp>
#include <boost/asio/awaitable.hpp>
#include <stdexcept>
#include <string>
#include <chrono>
#include <functional>



class BellardCompletionAPI {
    boost::asio::ip::tcp::resolver resolver;
    boost::asio::ssl::context ctx{boost::asio::ssl::context::tlsv12_client};
    std::chrono::minutes timeout = std::chrono::minutes(2);
    boost::beast::http::request_parser<boost::beast::http::string_body> header_parser;
    boost::asio::io_context& ex;

    struct Exception : public std::runtime_error {
        using std::runtime_error::runtime_error;
    };

public:
    BellardCompletionAPI(boost::asio::io_context& ex) : ex(ex), resolver(ex) {}
    static constexpr size_t bufSize = 16;

    boost::asio::awaitable<std::string> complete(const std::string& prompt, char delim);
};
#endif
