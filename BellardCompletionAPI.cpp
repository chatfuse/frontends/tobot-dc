#include "BellardCompletionAPI.hpp"

#include <string_view>
#include <sstream>
#include <array>
#include <memory>
#include <boost/asio/use_awaitable.hpp>



boost::asio::awaitable<std::string> BellardCompletionAPI::complete(const std::string& prompt, char delim) {
    using streamT = boost::beast::ssl_stream<boost::beast::tcp_stream>;
    auto stream = std::make_shared<streamT>(ex, ctx);

    // Set SNI Hostname (many hosts need this to handshake successfully)
    SSL_set_tlsext_host_name(stream->native_handle(), "bellard.org");

    // Set up an HTTP GET request message
    auto req = std::make_shared<boost::beast::http::request<boost::beast::http::string_body>>();
    req->version(11);
    req->method(boost::beast::http::verb::post);
    req->target("/textsynth/api/v1/engines/gptj_6B/completions");
    req->set(boost::beast::http::field::host, "bellard.org");
    req->set(boost::beast::http::field::user_agent, BOOST_BEAST_VERSION_STRING);
    req->set(boost::beast::http::field::content_type, "application/json");
    req->body() = nlohmann::json({
                                     {"prompt", prompt},
                                     {"stream", true}
                                 }).dump();
    req->prepare_payload();

    // Look up the domain name
    auto results = co_await resolver.async_resolve("bellard.org", "443", boost::asio::use_awaitable);

    // Make the connection on the IP address we get from a lookup
    co_await boost::beast::get_lowest_layer(*stream).async_connect(results, boost::asio::use_awaitable);

    // Perform the SSL handshake
    co_await stream->async_handshake(boost::asio::ssl::stream_base::client, boost::asio::use_awaitable);

    // Set a timeout on the stream
    boost::beast::get_lowest_layer(*stream).expires_after(timeout);

    // Send the HTTP request to the remote host
    co_await boost::beast::http::async_write(*stream, *req, boost::asio::use_awaitable);

    // Loop until all everything we need was read
    std::string fres;
    {
        std::array<char, bufSize> rawBuffer;
        std::string lineBuffer;
        std::ostringstream outputBuffer;
        bool finished = false;

        while (!finished) {
            auto bytes_read = co_await stream->async_read_some(boost::asio::mutable_buffer(rawBuffer.data(), rawBuffer.size()), boost::asio::use_awaitable);
            if (bytes_read) {
                auto strBuffer = std::string_view{rawBuffer.data(), bytes_read};
                for (const char c : strBuffer) {
                    if (c == '\r') {
                        continue;
                    } else if (c == '\n') {
                        // Process lineBuffer
                        if (!lineBuffer.empty() && (lineBuffer)[0] == '{') {
                            try {
                                auto jsonData = nlohmann::json::parse(std::move(lineBuffer));
                                if (jsonData.contains("error")) {
                                    throw Exception(jsonData["error"]);
                                    break;
                                }
                                bool reachedEnd = jsonData["reached_end"];
                                for (const char c : std::string(jsonData["text"])) {
                                    if (c == delim) {
                                        reachedEnd = true;
                                        break;
                                    } else {
                                        outputBuffer << c;
                                    }
                                }
                                if (reachedEnd) {
                                    fres = outputBuffer.str();
                                    finished = true;
                                    break;
                                }
                            } catch (nlohmann::json::exception&) {
                                // Ignore (for now)
                            }
                        }
                        // Reset lineBuffer
                        lineBuffer = std::string();
                    } else {
                        lineBuffer.push_back(c);
                    }
                }
            }
        }
    }

    stream->async_shutdown([] (...) {});
    co_return fres;
}
