#include "tobot.hpp"

#include <memory>
#include <coroutine>
#include <unistd.h>

#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/address.hpp>
using namespace std;



int main(int argc, char **argv) {
    // Check args
    if (argc == 1) {
        std::cout << "Usage: " << argv[0] << " <bot token>" << std::endl;
        return EXIT_FAILURE;
    }

    // Create io service
    boost::asio::io_service io;

    // Set up client
    auto client = std::make_shared<MyClient>(io, ChatFuse::Discord::Settings{
                                                 .bot_token = argv[1],
                                                 .intents = ChatFuse::Discord::intents::guild_messages | ChatFuse::Discord::intents::guilds
                                             });
    client->detach();

    // Erase bot token from argv (so it's no longer visible in /proc/{pid}/cmdline)
    memset(argv[1], 0, strlen(argv[1]));

    // Run!!!
    io.run();
    return EXIT_SUCCESS;
}
