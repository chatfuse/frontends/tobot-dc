#ifdef MODEL
#   include "ai.hpp"
#else
#   include "BellardCompletionAPI.hpp"
#endif

#include <string>
#include <tuple>
#include <exception>
#include <unordered_map>

#include <dcboost/discord.hpp>
#include <dcboost/snowflake.hpp>
#include <boost/asio/io_service.hpp>
#include <logger.hpp>
#include <fmt/format.h>

using fmt::operator""_format;



struct MyClient : public ChatFuse::Discord::Client {
    std::unordered_map<std::string, std::tuple<std::string, size_t>> channels;
    QLog::Logger<MyClient> logger;
    struct {
        ChatFuse::Discord::Snowflake id;
        std::string name;
    } self;
#   ifdef MODEL
    Ai
#   else
    BellardCompletionAPI
#   endif
        ai;

    MyClient(boost::asio::io_service& io, const ChatFuse::Discord::Settings& settings = {})
        : ChatFuse::Discord::Client(io, settings),
          ai(
#         ifdef MODEL
             MODEL, "./gpt2_" MODEL ".bin"
#         else
             io
#         endif
          )
    {}

    boost::asio::awaitable<std::string> complete(const std::string& str, char delim) {
#       ifdef MODEL
        co_return ai.complete(str, [delim] (char c, ...) {
            return c != delim;
        });
#       else
        co_return co_await ai.complete(str, delim);
#       endif
    }

    virtual boost::asio::awaitable<void> intentHandler(const std::string& intent, const Json::Value& data) override {
        if (intent == "CHANNEL_CREATE") {
            // Add channel
            auto id = data["id"].asString();
            channels[id] = {"", 0};
            logger.log(QLog::Loglevel::debug, "Got channel {}"_format(id));
        } else if (intent == "GUILD_CREATE") [[unlikely]] {
            logger.log(QLog::Loglevel::debug, "Got guild {}"_format(data["id"].asString()));
            for (const auto& channel : data["channels"]) {
                co_await intentHandler("CHANNEL_CREATE", channel);
            }
        } else if (intent == "READY") [[unlikely]] {
            auto& user = data["user"];
            self.id = user["id"];
            self.name = user["username"].asString();
        } else if (intent == "MESSAGE_CREATE") [[likely]] {
            auto& author_data = data["author"];
            if (!author_data["bot"].asBool()) {
                ChatFuse::Discord::Snowflake author = std::move(author_data["id"]),
                                             channel = std::move(data["channel_id"]);
                std::string content = std::move(data["content"]).asString();
                logger.log(QLog::Loglevel::debug, "Received message from {} in {}: {}"_format(author.uint(), channel.uint(), content));
                // Find channel log
                auto res = channels.find(channel.str());
                if (res != channels.end()) {
                    // Append to backlog and increase message counter
                    auto& [backlog, msg_count] = res->second;
                    backlog += "<{}> {}\n"_format(author_data["name"].asString(), content);
                    msg_count++;
                    // If message contained name or "random" occasion and at least 4 messages were received, respond
                    if ((content.find(self.id.str()) != std::string_view::npos || content.find(self.name) != std::string_view::npos || (content.size() + msg_count) % 4 == 0) && msg_count >= 4) {
                        // "Inspire" bot to respond here
                        backlog += "<{}>"_format(self.name);
                        try {
                            // Let the AI do its stuff...
                            auto message = co_await complete(backlog, '\n');
                            // Strip ' ' from message
                            if (message[0] == ' ') [[likely]] {
                                message.erase(0, 1);
                            }
                            // Append to backlog
                            backlog += ' ' + message + '\n';
                            // Send message
                            Json::Value call;
                            call["content"] = message;
                            co_await api.call(boost::beast::http::verb::post, "/channels/{}/messages"_format(channel.uint()), call);
                            // Log message
                            logger.log(QLog::Loglevel::debug, "Sending message to {}: {}"_format(channel.uint(), message));
                        } catch (std::exception& e) {
                            logger.log(QLog::Loglevel::error, "Error from api: {}"_format(e.what()));
                        }
                    }
                }
            }
        }
    }
};
